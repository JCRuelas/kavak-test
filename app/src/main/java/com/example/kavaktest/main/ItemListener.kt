package com.example.kavaktest.main

typealias ItemListener = ((index: Int, id: String) -> Unit)