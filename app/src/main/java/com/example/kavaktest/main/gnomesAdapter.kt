package com.example.kavaktest.main


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.kavaktest.R
import com.example.kavaktest.models.Gnome

class gnomesAdapter(private val gnomes: MutableList<com.example.kavaktest.models.Gnome>,
private val selection: ItemListener) : RecyclerView.Adapter<gnomesAdapter.Gnome>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Gnome {
        return Gnome(
            LayoutInflater.from(parent.context).inflate(R.layout.item_gnome, parent, false),
            this
        )
    }

    override fun getItemCount(): Int = gnomes.size

    override fun onBindViewHolder(holder: Gnome, position: Int) {
        holder.name.text = gnomes[position].name
    }


    fun itemClicked(index: Int){
        this.selection.invoke(index, gnomes[index].id)
    }

    class Gnome(itemView: View, private val adapter: gnomesAdapter): RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val name: TextView = itemView.findViewById(R.id.txt_name)

        init {
            itemView.setOnClickListener(this)
        }
        override fun onClick(v: View?) = adapter.itemClicked(adapterPosition)
    }

}