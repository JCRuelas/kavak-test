package com.example.kavaktest.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.kavaktest.R
import com.example.kavaktest.models.Gnome

class MainActivity : AppCompatActivity() {

    private val gnomes by lazy {
        intent.getBundleExtra("gnomes")
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}