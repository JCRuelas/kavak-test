package com.example.kavaktest.splash

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.kavaktest.api.Repository
import com.example.kavaktest.models.Gnome
import kotlinx.coroutines.launch

class SplashViewModel: ViewModel() {

    var gnomesList = MutableLiveData<List<Gnome>>()
    var repository = Repository()

    fun getGnomes(){
        viewModelScope.launch {
            gnomesList.value = repository.getGnomes()
        }
    }

    fun saveGnomes(gnomes: List<Gnome>){
        viewModelScope.launch {
            repository.saveGnomes(gnomes)
        }
    }


}