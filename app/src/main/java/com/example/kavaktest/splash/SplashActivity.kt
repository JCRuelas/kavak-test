package com.example.kavaktest.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.kavaktest.R
import com.example.kavaktest.main.MainActivity
import com.example.kavaktest.models.Gnome

class SplashActivity: AppCompatActivity() {

    private val viewModel by lazy {
        ViewModelProvider(this).get(SplashViewModel::class.java)
    }

    private val gnomesObserver = Observer<List<Gnome>> {
//        viewModel.saveGnomes(it)
        val intent = Intent(this, MainActivity::class.java).apply {
            bundleOf("gnomes" to it)
        }
        startActivity(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setObservers()
        retrieveBrastlewark()

    }

    private fun setObservers() {
        viewModel.gnomesList.observe(this, gnomesObserver)
    }

    private fun retrieveBrastlewark(){
        viewModel.getGnomes()
    }



}