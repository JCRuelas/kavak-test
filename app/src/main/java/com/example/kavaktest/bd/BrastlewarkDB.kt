package com.example.kavaktest.bd

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.kavaktest.models.Gnome


@Database(
    entities = [Gnome::class],
    version = 1,
    exportSchema = false
)
abstract class BrastlewarkDB: RoomDatabase() {

    abstract fun gnomeDao(): GnomeDao


    companion object {

        @Volatile
        private var instance: BrastlewarkDB? = null

        fun getInstance(context: Context): BrastlewarkDB {
            return instance
                ?: synchronized(this) {
                    instance
                        ?: buildDatabase(
                            context
                        ).also { instance = it }
                }
        }

        private fun buildDatabase(context: Context): BrastlewarkDB {
            return Room.databaseBuilder(
                context, BrastlewarkDB::class.java,
                this::class.java.simpleName

            ).build()
        }
    }
}