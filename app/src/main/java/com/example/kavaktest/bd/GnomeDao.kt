package com.example.kavaktest.bd

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.kavaktest.models.Gnome


@Dao
interface GnomeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllGnomes(gnomes:List<Gnome>)

    @Query("SELECT * FROM Gnome")
    suspend fun getAllGnomes(): List<Gnome>



}