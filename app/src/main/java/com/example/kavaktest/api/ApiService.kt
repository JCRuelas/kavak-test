package com.example.kavaktest.api

import com.example.kavaktest.api.response.BrastlewarkResponse
import retrofit2.http.GET
import retrofit2.Response

interface ApiService {

    @GET("data.json")
    suspend fun getGnomes(
    ): Response<BrastlewarkResponse>

}