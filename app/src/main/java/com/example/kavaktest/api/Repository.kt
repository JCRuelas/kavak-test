package com.example.kavaktest.api

import android.util.Log
import com.example.kavaktest.App
import com.example.kavaktest.models.Gnome
import com.example.kavaktest.R
import com.example.kavaktest.bd.BrastlewarkDB
import com.example.kavaktest.bd.GnomeDao
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

open class Repository {

    private val apiService = ApiManager.makeRetrofitService(App.instance.getString(R.string.BASE_URL))
    private val gnomesDao: GnomeDao = BrastlewarkDB.getInstance(App.instance).gnomeDao()


    suspend fun getGnomes(): List<Gnome>{

        val response = apiService.getGnomes()
        var result = listOf<Gnome>()

        withContext(Dispatchers.Main){
            if  (response.isSuccessful){
                result = response.body()?.brastlewark?.gnomes!!
            } else {
                Log.e("RETROFIT_ERROR", response.code().toString())
            }
        }

       return result
    }

    suspend fun saveGnomes(gnomes: List<Gnome>){
        gnomesDao.insertAllGnomes(gnomes)
    }


}