package com.example.kavaktest.api.response

import com.example.kavaktest.models.Brastlewark
import com.google.gson.annotations.SerializedName

data class BrastlewarkResponse(

    @SerializedName("Brastlewark")
    val brastlewark: Brastlewark
)
