package com.example.kavaktest.models

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Relation


@Entity
data class Brastlewark(
    @Embedded
    @Relation(
        parentColumn = "id",
        entityColumn = "friends"
    )
    val gnomes: List<Gnome>
)
