package com.example.kavaktest.models

data class Friend(
    val id: Int,
    val name: String
)
