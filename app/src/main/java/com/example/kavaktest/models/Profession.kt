package com.example.kavaktest.models

data class Profession(
    val id: Int,
    val name: String
)
