package com.example.kavaktest.models

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation

@Entity
data class Gnome(
    @Embedded
    @PrimaryKey
    val id: String,
    val name: String,
    val thumbnail: String,
    val age: Int,
    val weight: Double,
    val height: Double,
    val hair_color: String,
    @Relation(
        parentColumn = "id",
        entityColumn = "professions"
    )
    var professions: List<Profession>,
    @Relation(
        parentColumn = "id",
        entityColumn = "friends"
    )
    var friends: List<Friend>
)
